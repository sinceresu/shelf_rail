#/bin/bash

# ****** Run ******

# cd /home/nvidia/shelf_rail

# params
echo "start params: $0 $*"
echo "num: $#"
echo ""

PRG="$0"
while [ -h "$PRG" ] ; do
  ls=`ls -ld "$PRG"`
  link=`expr "$ls" : '.*-> \(.*\)$'`
  if expr "$link" : '/.*' > /dev/null; then
    PRG="$link"
  else
    PRG=`dirname "$PRG"`/"$link"
  fi
done

# variable
PRGDIR=`dirname "$PRG"`
BLOOP=1

# permission
# echo 'nvidia' | sudo -S chmod 777 /dev/ttyUSB*

# ctrl + c
trap "onCtrlC" INT
function onCtrlC() {
  BLOOP=0
  echo "Ctrl+C is captured"
}

echo ""
echo "[YDRobot] Configuring dependencies ......"

USER_NAME=nvidia
#
echo "USER_NAME" $USER_NAME

# kinetic/melodic/noetic
ROS_SYS=melodic
LOCAL_IP=192.168.1.251
MASTER_IP=192.168.1.251
WORKSPACE_PATH=`echo /home/nvidia/xifeihe_robot`

#
echo "ROS_SYS" $ROS_SYS
echo "LOCAL_IP" $LOCAL_IP
echo "MASTER_IP" $MASTER_IP
echo "WORKSPACE_PATH" $WORKSPACE_PATH
echo ""

# env
export ROS_IP=$LOCAL_IP
export ROS_HOSTNAME=$LOCAL_IP
export ROS_MASTER_URI=http://$MASTER_IP:11311
#
source /opt/ros/$ROS_SYS/setup.bash
source $WORKSPACE_PATH/devel/setup.bash

# loop
count=0
max=36000
while [ $BLOOP -ge 1 ] ; do
  # +1
  count=$(( ${count} + 1 ))
  # if (( "$count" >= "$max" )) then
  if [ "$count" -ge "$max" ]; then
    count=0
    echo "rosnode kill /ir1/streaming_service_node"
    rosnode kill /ir1/streaming_service_node
    sleep 1
    echo "rosnode kill /ir2/streaming_service_node"
    rosnode kill /ir2/streaming_service_node
    sleep 10
    echo "roslaunch streaming_service multi_streaming_service_node.launch"
    roslaunch streaming_service multi_streaming_service_node.launch &
    sleep 1
  fi
  sleep 1
done
