# 消息转换
roslaunch slide_message_bridge slide_message_bridge.launch
# 电机驱动
roslaunch slide_control_drive slide_control_drive.launch
# 温度数据
roslaunch fixed_hk_nosdk multi_temp_node.launch
# 可见光解码
roslaunch fixed_decoder multi_visible.launch
# 红外解码
roslaunch fixed_decoder multi_infrared.launch
# 相机定位
roslaunch localize_service localize_service.launch
# 数据同步
roslaunch undistort_service multi_undistort_service_node.launch
# 推流服务
roslaunch streaming_service multi_streaming_service_node.launch
# 任务服务
roslaunch fixed_slide_infrared client.launch
# 任务控制
roslaunch fixed_control slide_control.launch
