# fixed_slide_infrared
接收控制的消息,控制固定式设备按任务运动并采集红外图像      
#  话题
##  云台变换
Topic: /fixed/platform/transfer  
Type: fixed_msg/platform_transfer  
```json
int32 flag
string data
```
##  开始着色任务
Topic: /infrared_survey_parm  
Type: fixed_msg::inspected_result    
```json
int32 camid
int32 picid
float32 x
float32 y
float32 z
uint8[] equipimage
uint8[] nameplates
string equipid
string result
bool success
```
#  启动
```json
roslanuch fixed_slide_infrared client.launch
```