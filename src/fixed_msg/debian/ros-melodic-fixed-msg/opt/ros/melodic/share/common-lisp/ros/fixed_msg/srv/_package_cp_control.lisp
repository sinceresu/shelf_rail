(cl:in-package fixed_msg-srv)
(cl:export '(ID-VAL
          ID
          ACTION-VAL
          ACTION
          TYPE-VAL
          TYPE
          VALUE-VAL
          VALUE
          ALLVALUE-VAL
          ALLVALUE
          RESULT-VAL
          RESULT
))