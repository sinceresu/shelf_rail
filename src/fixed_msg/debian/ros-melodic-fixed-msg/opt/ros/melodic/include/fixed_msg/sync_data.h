// Generated by gencpp from file fixed_msg/sync_data.msg
// DO NOT EDIT!


#ifndef FIXED_MSG_MESSAGE_SYNC_DATA_H
#define FIXED_MSG_MESSAGE_SYNC_DATA_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace fixed_msg
{
template <class ContainerAllocator>
struct sync_data_
{
  typedef sync_data_<ContainerAllocator> Type;

  sync_data_()
    : stamp()
    , version(0)
    , device_id(0)
    , pos_x(0.0)
    , pos_y(0.0)
    , pos_z(0.0)
    , qua_x(0.0)
    , qua_y(0.0)
    , qua_z(0.0)
    , qua_w(0.0)
    , horizontal(0)
    , vertical(0)
    , temperature()
    , v_format()
    , v_data()  {
    }
  sync_data_(const ContainerAllocator& _alloc)
    : stamp(_alloc)
    , version(0)
    , device_id(0)
    , pos_x(0.0)
    , pos_y(0.0)
    , pos_z(0.0)
    , qua_x(0.0)
    , qua_y(0.0)
    , qua_z(0.0)
    , qua_w(0.0)
    , horizontal(0)
    , vertical(0)
    , temperature(_alloc)
    , v_format(_alloc)
    , v_data(_alloc)  {
  (void)_alloc;
    }



   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _stamp_type;
  _stamp_type stamp;

   typedef uint8_t _version_type;
  _version_type version;

   typedef uint8_t _device_id_type;
  _device_id_type device_id;

   typedef float _pos_x_type;
  _pos_x_type pos_x;

   typedef float _pos_y_type;
  _pos_y_type pos_y;

   typedef float _pos_z_type;
  _pos_z_type pos_z;

   typedef float _qua_x_type;
  _qua_x_type qua_x;

   typedef float _qua_y_type;
  _qua_y_type qua_y;

   typedef float _qua_z_type;
  _qua_z_type qua_z;

   typedef float _qua_w_type;
  _qua_w_type qua_w;

   typedef int32_t _horizontal_type;
  _horizontal_type horizontal;

   typedef int32_t _vertical_type;
  _vertical_type vertical;

   typedef std::vector<uint8_t, typename ContainerAllocator::template rebind<uint8_t>::other >  _temperature_type;
  _temperature_type temperature;

   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _v_format_type;
  _v_format_type v_format;

   typedef std::vector<uint8_t, typename ContainerAllocator::template rebind<uint8_t>::other >  _v_data_type;
  _v_data_type v_data;





  typedef boost::shared_ptr< ::fixed_msg::sync_data_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::fixed_msg::sync_data_<ContainerAllocator> const> ConstPtr;

}; // struct sync_data_

typedef ::fixed_msg::sync_data_<std::allocator<void> > sync_data;

typedef boost::shared_ptr< ::fixed_msg::sync_data > sync_dataPtr;
typedef boost::shared_ptr< ::fixed_msg::sync_data const> sync_dataConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::fixed_msg::sync_data_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::fixed_msg::sync_data_<ContainerAllocator> >::stream(s, "", v);
return s;
}


template<typename ContainerAllocator1, typename ContainerAllocator2>
bool operator==(const ::fixed_msg::sync_data_<ContainerAllocator1> & lhs, const ::fixed_msg::sync_data_<ContainerAllocator2> & rhs)
{
  return lhs.stamp == rhs.stamp &&
    lhs.version == rhs.version &&
    lhs.device_id == rhs.device_id &&
    lhs.pos_x == rhs.pos_x &&
    lhs.pos_y == rhs.pos_y &&
    lhs.pos_z == rhs.pos_z &&
    lhs.qua_x == rhs.qua_x &&
    lhs.qua_y == rhs.qua_y &&
    lhs.qua_z == rhs.qua_z &&
    lhs.qua_w == rhs.qua_w &&
    lhs.horizontal == rhs.horizontal &&
    lhs.vertical == rhs.vertical &&
    lhs.temperature == rhs.temperature &&
    lhs.v_format == rhs.v_format &&
    lhs.v_data == rhs.v_data;
}

template<typename ContainerAllocator1, typename ContainerAllocator2>
bool operator!=(const ::fixed_msg::sync_data_<ContainerAllocator1> & lhs, const ::fixed_msg::sync_data_<ContainerAllocator2> & rhs)
{
  return !(lhs == rhs);
}


} // namespace fixed_msg

namespace ros
{
namespace message_traits
{





template <class ContainerAllocator>
struct IsFixedSize< ::fixed_msg::sync_data_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::fixed_msg::sync_data_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct IsMessage< ::fixed_msg::sync_data_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::fixed_msg::sync_data_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::fixed_msg::sync_data_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::fixed_msg::sync_data_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::fixed_msg::sync_data_<ContainerAllocator> >
{
  static const char* value()
  {
    return "7bc7618cef60033e2d78462cb5d33ffd";
  }

  static const char* value(const ::fixed_msg::sync_data_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x7bc7618cef60033eULL;
  static const uint64_t static_value2 = 0x2d78462cb5d33ffdULL;
};

template<class ContainerAllocator>
struct DataType< ::fixed_msg::sync_data_<ContainerAllocator> >
{
  static const char* value()
  {
    return "fixed_msg/sync_data";
  }

  static const char* value(const ::fixed_msg::sync_data_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::fixed_msg::sync_data_<ContainerAllocator> >
{
  static const char* value()
  {
    return "string stamp\n"
"uint8 version\n"
"uint8 device_id\n"
"float32 pos_x\n"
"float32 pos_y\n"
"float32 pos_z\n"
"float32 qua_x\n"
"float32 qua_y\n"
"float32 qua_z\n"
"float32 qua_w\n"
"int32 horizontal\n"
"int32 vertical\n"
"uint8[] temperature\n"
"string v_format\n"
"uint8[] v_data\n"
;
  }

  static const char* value(const ::fixed_msg::sync_data_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::fixed_msg::sync_data_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.stamp);
      stream.next(m.version);
      stream.next(m.device_id);
      stream.next(m.pos_x);
      stream.next(m.pos_y);
      stream.next(m.pos_z);
      stream.next(m.qua_x);
      stream.next(m.qua_y);
      stream.next(m.qua_z);
      stream.next(m.qua_w);
      stream.next(m.horizontal);
      stream.next(m.vertical);
      stream.next(m.temperature);
      stream.next(m.v_format);
      stream.next(m.v_data);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct sync_data_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::fixed_msg::sync_data_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::fixed_msg::sync_data_<ContainerAllocator>& v)
  {
    s << indent << "stamp: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.stamp);
    s << indent << "version: ";
    Printer<uint8_t>::stream(s, indent + "  ", v.version);
    s << indent << "device_id: ";
    Printer<uint8_t>::stream(s, indent + "  ", v.device_id);
    s << indent << "pos_x: ";
    Printer<float>::stream(s, indent + "  ", v.pos_x);
    s << indent << "pos_y: ";
    Printer<float>::stream(s, indent + "  ", v.pos_y);
    s << indent << "pos_z: ";
    Printer<float>::stream(s, indent + "  ", v.pos_z);
    s << indent << "qua_x: ";
    Printer<float>::stream(s, indent + "  ", v.qua_x);
    s << indent << "qua_y: ";
    Printer<float>::stream(s, indent + "  ", v.qua_y);
    s << indent << "qua_z: ";
    Printer<float>::stream(s, indent + "  ", v.qua_z);
    s << indent << "qua_w: ";
    Printer<float>::stream(s, indent + "  ", v.qua_w);
    s << indent << "horizontal: ";
    Printer<int32_t>::stream(s, indent + "  ", v.horizontal);
    s << indent << "vertical: ";
    Printer<int32_t>::stream(s, indent + "  ", v.vertical);
    s << indent << "temperature[]" << std::endl;
    for (size_t i = 0; i < v.temperature.size(); ++i)
    {
      s << indent << "  temperature[" << i << "]: ";
      Printer<uint8_t>::stream(s, indent + "  ", v.temperature[i]);
    }
    s << indent << "v_format: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.v_format);
    s << indent << "v_data[]" << std::endl;
    for (size_t i = 0; i < v.v_data.size(); ++i)
    {
      s << indent << "  v_data[" << i << "]: ";
      Printer<uint8_t>::stream(s, indent + "  ", v.v_data[i]);
    }
  }
};

} // namespace message_operations
} // namespace ros

#endif // FIXED_MSG_MESSAGE_SYNC_DATA_H
