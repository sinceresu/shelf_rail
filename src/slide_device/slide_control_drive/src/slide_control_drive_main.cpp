#include <stdio.h>
#include <math.h>
#include <csignal>
#include <unistd.h>
#include <string>

#include "slide_control_drive_node.hpp"
#include "slide_control_connect.hpp"

void sig_handler(int sig)
{
    if (sig == SIGINT)
    {
        exit(0);
    }
}

namespace slide_control_drive
{

    void run()
    {
        CSlideControlDriveNode::NodeOptions node_options;
        ros::param::get("~device_id", node_options.device_id);
        ros::param::get("~device_ip", node_options.device_ip);
        ros::param::get("~device_port", node_options.device_port);
        //
        ros::param::get("~data_length", node_options.data_length);
        //
        ros::param::get("~action_timeout", node_options.action_timeout);
        ros::param::get("~position_tolerance", node_options.position_tolerance);
        ros::param::get("~max_length_x", node_options.max_length_x);
        ros::param::get("~max_length_y", node_options.max_length_y);
        ros::param::get("~max_speed_x", node_options.max_speed_x);
        ros::param::get("~max_speed_y", node_options.max_speed_y);
        ros::param::get("~min_speed_x", node_options.min_speed_x);
        ros::param::get("~min_speed_y", node_options.min_speed_y);
        //
        ros::param::get("~position_pub", node_options.position_pub);
        ros::param::get("~voltage_pub", node_options.voltage_pub);
        ros::param::get("~position_action", node_options.position_action);

        CSlideControlDriveNode slide_control_drive_node(node_options);

        ROS_INFO("slide_control_drive node started.");

        ros::spin();
        // ros::Rate loop_rate(1);
        // while (ros::ok())
        // {
        //     ros::spinOnce();
        //     loop_rate.sleep();
        // }
    }

}

int main(int argc, char **argv)
{
    signal(SIGINT, sig_handler);

    setlocale(LC_CTYPE, "zh_CN.utf8");
    setlocale(LC_ALL, "");

    std::string run_path = "";
    char *buffer;
    if ((buffer = getcwd(NULL, 0)) == NULL)
    {
        run_path = ".";
    }
    else
    {
        run_path = buffer;
        free(buffer);
    }

    std::string pack_path = "";
    pack_path = ros::package::getPath("slide_control_drive");

    ros::init(argc, argv, "slide_control_drive");
    ros::Time::init();

    slide_control_drive::run();

    ros::shutdown();

    return 0;
}
