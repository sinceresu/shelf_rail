#ifndef slide_control_drive_node_HPP
#define slide_control_drive_node_HPP

#pragma once
#include <queue>
#include <mutex>
#include "ros/ros.h"
#include "ros/package.h"

#include "std_msgs/Int32.h"
#include "std_msgs/String.h"
#include "geometry_msgs/PoseStamped.h"
#include "actionlib/server/simple_action_server.h"
#include "slide_control_msgs/Voltage.h"
#include "slide_control_msgs/PositionAction.h"

#include "slide_control_connect.hpp"

namespace slide_control_drive
{

    class CSlideControlDriveNode
    {
    public:
        typedef struct _NodeOptions
        {
            std::string device_id;
            std::string device_ip;
            int device_port;
            //
            int data_length;
            //
            double action_timeout;
            double position_tolerance;
            double max_length_x;
            double max_length_y;
            double max_speed_x;
            double max_speed_y;
            double min_speed_x;
            double min_speed_y;
            //
            std::string position_pub;
            std::string voltage_pub;
            std::string position_action;
        } NodeOptions;

    public:
        typedef actionlib::SimpleActionServer<slide_control_msgs::PositionAction> PositionServer;

    public:
        CSlideControlDriveNode(NodeOptions _node_options);
        ~CSlideControlDriveNode();

    private:
        NodeOptions node_options;
        void test_send_fun(void *p);
        void test_recv_fun(void *p);

    private:
        ros::NodeHandle nh;
        ros::NodeHandle private_nh;

        void LaunchPublishers();
        void LaunchSubscribers();
        void LaunchService();
        void LaunchActions();

        ros::Publisher slide_position_publisher;
        void publisher_slide_position(double x, double y);
        ros::Publisher slide_voltage_publisher;
        void publisher_slide_voltage(unsigned int v);
        //
        ros::Subscriber subscriber_test;
        void test_Callback(const std_msgs::Int32::ConstPtr &msg);
        //
        PositionServer position_action;
        void position_executeCB(const slide_control_msgs::PositionGoalConstPtr &goal);

    private:
        CSlideControlConnect slide;

        bool b_reset_position_x;
        bool b_reset_position_y;
        bool b_query_position;
        bool b_set_position;
        bool b_query_voltage;
        int query_count;
        double position_x;
        double position_y;
        double speed_x;
        double speed_y;
        unsigned int voltage;
        bool slide_connect(std::string _ip, int _port, int _protocol_size);
        void device_query(void *p);
        void parse_protocol(unsigned char cmdType, unsigned char *recvBuf, int recvLen);
        void recv_data_thread(void *p);
    };

}

#endif
