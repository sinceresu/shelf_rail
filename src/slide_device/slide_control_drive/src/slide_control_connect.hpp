#ifndef slide_control_connect_HPP
#define slide_control_connect_HPP

#pragma once
#include <thread>
#include <mutex>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <functional>
#include <unistd.h>
#include <string.h>

#define DATA16_GET_HIGH_BYTE(data) (((data) >> 8) & 0xFF)
#define DATA16_GET_LOW_BYTE(data) ((data)&0xFF)

#define DATA32_GET_BYTE3(data) (((data) >> 24) & 0xFF)
#define DATA32_GET_BYTE2(data) (((data) >> 16) & 0xFF)
#define DATA32_GET_BYTE1(data) (((data) >> 8) & 0xFF)
#define DATA32_GET_BYTE0(data) ((data)&0xFF)

namespace slide_control_drive
{

    enum
    {
        RESET_POSITION = 0x00,
        QUERY_POSITION = 0x01,
        SET_POSITION = 0x03,
        QUERY_VOLTAGE = 0X08
    };

    class CSlideControlConnect
    {
    public:
        CSlideControlConnect();
        CSlideControlConnect(std::string _ip, int _port);
        ~CSlideControlConnect();

    private:
        std::string device_ip;
        int device_port;
        //
        int sock_client;
        int connect_status;
        bool b_heart_beat;
        static unsigned int counter;
        //
        unsigned int data_length;
        //
        std::mutex send_mtx;
        std::mutex recv_mtx;
        //
        unsigned int min_cmd_interval_msec;
        //
        unsigned int send_fail_count;
        unsigned int recv_fail_count;

    private:
        void heart_beat(unsigned int sec = 0);
        //
        bool sendData(const unsigned char cmdType, const unsigned char *cmdBuf, const unsigned int cmdLen);
        bool recvData(unsigned char &cmdType, unsigned char *recvBuf, int &recvLen);

    public:
        bool device_connect(std::string _ip, int _port, unsigned int _sec = 0);
        void device_disconnect();
        void set_reconnection(unsigned int sec = 0);
        void set_protocol_size(unsigned int size = 12);
        int get_protocol_size();
        //
        bool readData(unsigned char &cmdType, unsigned char *recvBuf, int &recvLen);
        //
        std::string GetDeviceIp();
        int GetDevicePort();
        int GetConnectStatus();
        int SetConnectStatus(int status);
        //
        bool ResetDevicePositionX();
        bool ResetDevicePositionY();
        bool QueryDevicePosition();
        bool SetDevicePosition(int x, int y);
        bool QueryVoltage();
        bool SendTest(unsigned char cmdType, unsigned char *cmdBuf, unsigned int cmdLen);
        bool ReadTest(unsigned char &cmdType, unsigned char *recvBuf, int &recvLen);
    };

}

#endif
