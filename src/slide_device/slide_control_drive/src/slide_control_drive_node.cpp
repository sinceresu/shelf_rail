#include "slide_control_drive_node.hpp"

namespace slide_control_drive
{

    CSlideControlDriveNode::CSlideControlDriveNode(NodeOptions _node_options)
        : node_options(_node_options),
          position_action(nh, _node_options.position_action, boost::bind(&CSlideControlDriveNode::position_executeCB, this, _1), false)
    {
        b_reset_position_x = false;
        b_reset_position_y = false;
        b_query_position = false;
        b_set_position = false;
        b_query_voltage = false;

        LaunchPublishers();
        LaunchSubscribers();
        LaunchService();
        LaunchActions();

        slide_connect(_node_options.device_ip, _node_options.device_port, _node_options.data_length);

        std::thread recv_data(std::bind(&CSlideControlDriveNode::recv_data_thread, this, nullptr));
        recv_data.detach();
        std::thread slide_position(std::bind(&CSlideControlDriveNode::device_query, this, nullptr));
        slide_position.detach();
        // test
        // std::thread testRecv_thread(std::bind(&CSlideControlDriveNode::test_recv_fun, this, nullptr));
        // testRecv_thread.detach();
        // std::thread testSend_thread(std::bind(&CSlideControlDriveNode::test_send_fun, this, nullptr));
        // testSend_thread.detach();
    }

    CSlideControlDriveNode::~CSlideControlDriveNode()
    {
        /* code */
    }

    void CSlideControlDriveNode::test_send_fun(void *p)
    {
        sleep(1);
        int count = 0;
        ros::Rate loop_rate(10);
        while (ros::ok())
        {
            // unsigned char cmdHBuf[8] = {0xAA, 0x55, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
            // if (slide.SendTest(0x00, cmdHBuf, 8))
            // {
            //     printf("send cmd count : %d \r\n", count++);
            // }
            // if (slide.SendTest(0x00, cmdHBuf, 8))
            // {
            //     printf("send cmd count : %d \r\n", count++);
            // }
            //
            // for (size_t i = 0; i < 8; i++)
            // {
            //     // unsigned char cmdBuf[8] = {0xAA, 0x55, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
            //     unsigned char data = i;
            //     unsigned char cmdBuf[8] = {0xAA, 0x55, data, data, data, data, data, data};
            //     unsigned int checkNum = 0;
            //     for (size_t i = 0; i < (8 - 1); i++)
            //     {
            //         checkNum += cmdBuf[i];
            //     }
            //     cmdBuf[8 - 1] = DATA_GET_LOW_BYTE(checkNum);
            //     if (slide.SendTest(0x00, cmdBuf, 8))
            //     {
            //         printf("send cmd count : %d \r\n", count++);
            //     }
            // }
            //
            if (slide.QueryDevicePosition())
            {
                printf("send cmd count : %d \r\n", count++);
            }
            if (slide.SetDevicePosition(0.0, 0.0))
            {
                printf("send cmd count : %d \r\n", count++);
            }

            ros::spinOnce();
            loop_rate.sleep();
        }
    }

    void CSlideControlDriveNode::test_recv_fun(void *p)
    {
        int count = 0;
        ros::Rate loop_rate(1000);
        while (ros::ok())
        {
            unsigned char cmdType;
            unsigned char recvBuf[128];
            int recvLen = 128;
            if (slide.ReadTest(cmdType, recvBuf, recvLen))
            {
                printf("read cmd count : %d \r\n", count++);
            }
            //
            ros::spinOnce();
            loop_rate.sleep();
        }
    }

    void CSlideControlDriveNode::LaunchPublishers()
    {
        slide_position_publisher = nh.advertise<geometry_msgs::PoseStamped>(node_options.position_pub, 1);
        slide_voltage_publisher = nh.advertise<slide_control_msgs::Voltage>(node_options.voltage_pub, 1);
    }

    void CSlideControlDriveNode::LaunchSubscribers()
    {
        // subscriber_test = nh.subscribe("", 1, &CSlideControlDriveNode::test_Callback, this);
        /* code */
    }

    void CSlideControlDriveNode::LaunchService()
    {
        /* code */
    }

    void CSlideControlDriveNode::LaunchActions()
    {
        position_action.start();
    }

    void CSlideControlDriveNode::publisher_slide_position(double x, double y)
    {
        geometry_msgs::PoseStamped slide_position;
        slide_position.header.stamp = ros::Time::now();
        slide_position.pose.position.x = x;
        slide_position.pose.position.y = y;

        slide_position_publisher.publish(slide_position);
    }

    void CSlideControlDriveNode::publisher_slide_voltage(unsigned int v)
    {
        slide_control_msgs::Voltage slide_voltage;
        slide_voltage.value = v;
        // slide_voltage.ratio;

        slide_voltage_publisher.publish(slide_voltage);
    }

    void CSlideControlDriveNode::test_Callback(const std_msgs::Int32::ConstPtr &msg)
    {
        /* code */
    }

    void CSlideControlDriveNode::position_executeCB(const slide_control_msgs::PositionGoalConstPtr &goal)
    {
        ROS_INFO_STREAM("position_executeCB goal:\n"
                        << *goal);

        if (goal->action == 1)
        {
            ROS_INFO("reset position");
            b_reset_position_x = true;
            if (!slide.ResetDevicePositionX())
            {
                b_reset_position_x = false;
                slide_control_msgs::PositionResult result;
                result.result = 1;
                result.text = "send reset x cmd fail";
                // set the action state to aborted
                position_action.setAborted(result);
                ROS_ERROR("reset x position error");
                return;
            }
            usleep(1000 * 200); // 命令间隔时间
            b_reset_position_y = true;
            if (!slide.ResetDevicePositionY())
            {
                b_reset_position_y = false;
                slide_control_msgs::PositionResult result;
                result.result = 1;
                result.text = "send reset y cmd fail";
                // set the action state to aborted
                position_action.setAborted(result);
                ROS_ERROR("reset y position error");
                return;
            }
        }
        else if (goal->action == 2)
        {
            ROS_INFO("set position");
            b_set_position = true;
            if (isnan(goal->goal.position.x) || isnan(goal->goal.position.y) || isnan(goal->goal.position.z))
            {
                b_set_position = false;
                slide_control_msgs::PositionResult result;
                result.result = 1;
                result.text = "position cmd data error";
                // set the action state to aborted
                position_action.setAborted(result);
                ROS_ERROR("set position error");
            }
            // m -> mm * 10
            if (!slide.SetDevicePosition(goal->goal.position.x * 10000, goal->goal.position.y * 10000))
            {
                b_set_position = false;
                slide_control_msgs::PositionResult result;
                result.result = 1;
                result.text = "send set position cmd fail";
                // set the action state to aborted
                position_action.setAborted(result);
                ROS_ERROR("set position error");
                return;
            }
        }
        else
        {
            slide_control_msgs::PositionResult result;
            result.result = -1;
            result.text = "unknown cmd";
            // set the action state to aborted
            position_action.setAborted(result);
            ROS_ERROR("unknown cmd");
            return;
        }

        // helper variables
        double timeout_value = node_options.action_timeout;
        if (goal->action == 1)
        {
            double x_run_time = (node_options.max_length_x) / fabs(node_options.min_speed_x);
            double y_run_time = (node_options.max_length_y) / fabs(node_options.min_speed_y);
            double run_time = x_run_time > y_run_time ? x_run_time : y_run_time;
            timeout_value = run_time + node_options.action_timeout;
        }
        else if (goal->action == 2)
        {
            double x_run_time = (fabs(goal->goal.position.x - position_x) / fabs(node_options.min_speed_x));
            double y_run_time = (fabs(goal->goal.position.y - position_y) / fabs(node_options.min_speed_y));
            double run_time = x_run_time > y_run_time ? x_run_time : y_run_time;
            timeout_value = run_time + node_options.action_timeout;
        }
        else
        {
            timeout_value = node_options.action_timeout;
        }

        bool b_success = false;
        bool b_cancel = false;
        bool b_action_timeout = false;
        ros::Time start_time = ros::Time::now();
        ros::Time attemp_end = start_time + ros::Duration(timeout_value);

        // start executing the action
        ros::Rate r(2);
        while (ros::ok())
        {
            // check that preempt has not been requested by the client
            if (position_action.isPreemptRequested() || !ros::ok())
            {
                b_success = false;
                b_cancel = true;
                b_action_timeout = false;
                //
                slide_control_msgs::PositionResult result;
                result.result = 2;
                result.text = "preempted canceled";
                // set the action state to preempted
                position_action.setPreempted(result);
                break;
            }
            // check the action timed out
            if (ros::Time::now() > attemp_end)
            {
                b_success = false;
                b_cancel = false;
                b_action_timeout = true;
                //
                slide_control_msgs::PositionResult result;
                result.result = 3;
                result.text = "action timeout";
                // set the action state to aborted
                position_action.setAborted(result);
                ROS_ERROR("action timeout");
                break;
            }
            // check the action is completed
            if (goal->action == 1 && !b_reset_position_x && !b_reset_position_y)
            {
                b_success = true;
                b_cancel = false;
                b_action_timeout = false;
                ROS_INFO("reset position finish");
                break;
            }
            // if (goal->action == 2 && !b_set_position)
            if (goal->action == 2 && b_set_position)
            {
                // ROS_INFO("set position finish");
                if ((fabs(goal->goal.position.x - position_x) < node_options.position_tolerance) &&
                    (fabs(goal->goal.position.y - position_y) < node_options.position_tolerance))
                {
                    b_success = true;
                    b_cancel = false;
                    b_action_timeout = false;
                    ROS_INFO("set position finish");
                    break;
                }
                else
                {
                    // b_success = false;
                    // ROS_ERROR("set position fail");
                    // break;
                }
            }
            // feedback
            slide_control_msgs::PositionFeedback feedback;
            feedback.feedback.position.x = position_x;
            feedback.feedback.position.y = position_y;
            // publish the feedback
            position_action.publishFeedback(feedback);

            // this sleep is not necessary, the sequence is computed at 1 Hz for demonstration purposes
            r.sleep();
        }

        b_reset_position_x = false;
        b_reset_position_y = false;
        b_set_position = false;
        //
        if (b_success)
        {
            slide_control_msgs::PositionResult result;
            result.result = 0;
            result.text = "succeeded";
            // set the action state to succeeded
            position_action.setSucceeded(result);
        }
        else
        {
            if (!b_cancel && !b_action_timeout)
            {
                slide_control_msgs::PositionResult result;
                result.result = 1;
                result.text = "aborted";
                // set the action state to aborted
                position_action.setAborted(result);
            }
        }
    }

    bool CSlideControlDriveNode::slide_connect(std::string _ip, int _port, int _protocol_size)
    {
        slide.set_protocol_size(_protocol_size);
        return slide.device_connect(_ip, _port, 6);
    }

    void CSlideControlDriveNode::device_query(void *p)
    {
        usleep(1000 * 1000);
        static unsigned int interval = 0;
        ros::Rate loop_rate(5);
        while (ros::ok())
        {
            if (slide.QueryDevicePosition())
            {
                b_query_position = true;
            }
            //
            interval++;
            if (interval % 5 == 0)
            {
                interval = 0;
                if (slide.QueryVoltage())
                {
                    b_query_voltage = true;
                }
            }
            //
            query_count++;
            if (query_count > 130)
            {
                query_count = 0;
                slide.SetConnectStatus(-1);
            }
            //
            ros::spinOnce();
            loop_rate.sleep();
        }
    }

    void CSlideControlDriveNode::parse_protocol(unsigned char cmdType, unsigned char *recvBuf, int recvLen)
    {
        if (cmdType == (RESET_POSITION + 0x10))
        {
            if (recvBuf[3] == 0x01)
            {
                b_reset_position_x = false;
                ROS_INFO("recv x position reset finish");
            }
            if (recvBuf[3] == 0x02)
            {
                b_reset_position_y = false;
                ROS_INFO("recv y position reset finish");
            }
        }
        if (cmdType == (QUERY_POSITION + 0x10))
        {
            // mm -> m * 0.1
            position_x = ((recvBuf[6] << 24) | (recvBuf[5] << 16) | (recvBuf[4] << 8) | recvBuf[3]) * 0.0001;
            position_y = ((recvBuf[10] << 24) | (recvBuf[9] << 16) | (recvBuf[8] << 8) | recvBuf[7]) * 0.0001;
            publisher_slide_position(position_x, position_y);
            b_query_position = false;
            query_count = 0;
        }
        if (cmdType == (SET_POSITION + 0x10))
        {
            // b_set_position = false;
            ROS_INFO("recv position set finish");
        }
        if (cmdType == (QUERY_VOLTAGE + 0x10))
        {
            voltage = ((recvBuf[6] << 24) | (recvBuf[5] << 16) | (recvBuf[4] << 8) | recvBuf[3]);
            publisher_slide_voltage(voltage);
            b_query_voltage = false;
        }
    }

    void CSlideControlDriveNode::recv_data_thread(void *p)
    {
        usleep(1000 * 1000);
        int protocol_size = slide.get_protocol_size();
        ros::Rate loop_rate(10000);
        while (ros::ok())
        {
            unsigned char cmdType;
            // int recvLen = 128;
            int recvLen = protocol_size * 128;
            unsigned char recvBuf[recvLen];
            if (slide.readData(cmdType, recvBuf, recvLen))
            {
                parse_protocol(cmdType, recvBuf, recvLen);

                int data_size = recvLen;
                int offset = 0;
                offset += protocol_size;
                while ((data_size - offset) >= protocol_size)
                {
                    unsigned char data_buf[protocol_size];
                    memset(data_buf, 0x00, protocol_size);
                    strncpy((char *)data_buf, (char *)(recvBuf + offset), protocol_size);
                    if (data_buf[0] == 0xAA && data_buf[1] == 0x55)
                    {
                        cmdType = data_buf[2];
                        parse_protocol(cmdType, data_buf, protocol_size);
                    }
                    offset += protocol_size;
                }
            }
            else
            {
                usleep(1000 * 100);
            }
            //
            ros::spinOnce();
            loop_rate.sleep();
        }
    }

}
