#ifndef slide_message_bridge_node_HPP
#define slide_message_bridge_node_HPP

#pragma once
#include <queue>
#include <mutex>
#include "ros/ros.h"
#include "ros/package.h"

#include "Common.hpp"
#include "mqtt_base.hpp"
#include "ros_structs.hpp"
#include "mqtt_structs.hpp"

#include "fixed_msg/task.h"
#include "fixed_msg/task_control.h"
#include "fixed_msg/task_status.h"
#include "fixed_msg/control_mode.h"
#include "streaming_service_msgs/ColorizeInfo.h"

namespace slide_message_bridge
{
    class CSlideMessageBridgeNode
    {
    public:
        typedef struct _NodeOptions
        {
            std::string device_id;
            //
            std::string matt_address;
            std::string matt_clientId;
            std::string matt_username;
            std::string matt_password;
            //
            std::string control_mode_pub;
            std::string task_status_sub;
            std::string ir1_colorize_info_sub;
            std::string ir2_colorize_info_sub;
            std::string task_data_client;
            std::string task_control_client;
            //
            std::string mqtt_control_mode;
            std::string mqtt_task_data;
            std::string mqtt_task_command;
            std::string mqtt_task_status;
            std::string mqtt_colorize_info;
        } NodeOptions;

    public:
        CSlideMessageBridgeNode(NodeOptions _node_options);
        ~CSlideMessageBridgeNode();

    private:
        NodeOptions node_options;

    private:
        ros::NodeHandle nh;
        ros::NodeHandle private_nh;

        void LaunchPublishers();
        void LaunchSubscribers();
        void LaunchServices();
        void LaunchClinets();
        void LaunchActions();
        void LaunchSynchronizers();

        ros::Publisher control_mode_publisher;
        void publisher_control_mode(int device_id, int mode);

        // Subscriber
        ros::Subscriber task_status_subscriber;
        void task_status_Callback(const fixed_msg::task_status::ConstPtr &_msg);
        ros::Subscriber ir1_colorize_info_subscriber;
        void ir1_colorize_info_Callback(const streaming_service_msgs::ColorizeInfo::ConstPtr &_msg);
        ros::Subscriber ir2_colorize_info_subscriber;
        void ir2_colorize_info_Callback(const streaming_service_msgs::ColorizeInfo::ConstPtr &_msg);

        ros::ServiceClient task_data_client;
        bool call_task_data_service(int device_id, std::string data);
        ros::ServiceClient task_control_client;
        bool call_task_control_service(int device_id, int task_history_id, int flag);

    private:
        CMqttBase mqtt_base;
        int mqtt_init(std::string _address, std::string _clientId,
                      std::string _username, std::string _password);
        void MqttPublishers();
        void MqttSubscribers();
        int Message_mqttPublish(std::string topic, std::string data);

        void TaskDataDown_mqttCallback(const std::string &_topicName, const std::string &_message, const int &_length);
        void TaskCommandDown_mqttCallback(const std::string &_topicName, const std::string &_message, const int &_length);
        void ControlModeDown_mqttCallback(const std::string &_topicName, const std::string &_message, const int &_length);

        int TaskStatusUp_mqttPublish(std::string data);
        int ColorizeInfoUp_mqttPublish(std::string data);
    };
}

#endif