#include <stdio.h>
#include <math.h>
#include <csignal>
#include <unistd.h>
#include <string>

#include "slide_message_bridge_node.hpp"

void sig_handler(int sig)
{
    if (sig == SIGINT)
    {
        exit(0);
    }
}

namespace slide_message_bridge
{
    void run()
    {
        CSlideMessageBridgeNode::NodeOptions node_options;
        ros::param::get("~device_id", node_options.device_id);

        ros::param::get("~matt_address", node_options.matt_address);
        ros::param::get("~matt_clientId", node_options.matt_clientId);
        ros::param::get("~matt_username", node_options.matt_username);
        ros::param::get("~matt_password", node_options.matt_password);

        ros::param::get("~control_mode_pub", node_options.control_mode_pub);
        ros::param::get("~task_status_sub", node_options.task_status_sub);
        ros::param::get("~ir1_colorize_info_sub", node_options.ir1_colorize_info_sub);
        ros::param::get("~ir2_colorize_info_sub", node_options.ir2_colorize_info_sub);
        ros::param::get("~task_data", node_options.task_data_client);
        ros::param::get("~task_control", node_options.task_control_client);

        ros::param::get("~mqtt_control_mode", node_options.mqtt_control_mode);
        ros::param::get("~mqtt_task_data", node_options.mqtt_task_data);
        ros::param::get("~mqtt_task_command", node_options.mqtt_task_command);
        ros::param::get("~mqtt_task_status", node_options.mqtt_task_status);
        ros::param::get("~mqtt_colorize_info", node_options.mqtt_colorize_info);

        CSlideMessageBridgeNode slide_message_bridge_node(node_options);

        ROS_INFO("slide message bridge node started.");

        ros::spin();
        // ros::Rate loop_rate(1);
        // while (ros::ok())
        // {
        //     ros::spinOnce();
        //     loop_rate.sleep();
        // }
    }
}

int main(int argc, char **argv)
{
    signal(SIGINT, sig_handler);

    setlocale(LC_CTYPE, "zh_CN.utf8");
    setlocale(LC_ALL, "");

    std::string run_path = "";
    char *buffer;
    if ((buffer = getcwd(NULL, 0)) == NULL)
    {
        run_path = ".";
    }
    else
    {
        run_path = buffer;
        free(buffer);
    }

    std::string pack_path = "";
    pack_path = ros::package::getPath("slide_message_bridge");

    ros::init(argc, argv, "slide_message_bridge");
    ros::Time::init();

    slide_message_bridge::run();

    ros::shutdown();

    return 0;
}