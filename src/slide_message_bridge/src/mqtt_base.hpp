#ifndef mqtt_base_HPP
#define mqtt_base_HPP

#pragma once
#include <string>
#include <vector>
#include <map>
#include <thread>
#include <unistd.h>
#include <chrono>
#include <functional>
#include <string.h>
#include  "MQTTClient.h"

#define MQTT_BASE_VERSION "v1.0"

typedef std::function<void(const std::string &, const std::string &, const int &)> TopicMessageCallback;

typedef struct _topic_t
{
    std::string name;
    int qos;
    TopicMessageCallback callback;
} topic_t;

class CMqttBase
{
    typedef struct _MqttOptions
    {
        int reconnect;
        int reconnect_time;
    } MqttOptions;

public:
    CMqttBase();
    ~CMqttBase();

private:
    // --
    char *strtok_hier(char *str, char **saveptr);
    int count_hier_levels(const char *s);
    bool hash_check(char *s, size_t *len);
    bool sub_acl_check(const char *acl, const char *sub);
    //
    std::time_t get_time_stamp();
    //
    bool check_status;
    void connect_monitor(void *p);

private:
    std::string address;
    std::string clientId;
    std::string username;
    std::string password;

    MQTTClient client;
    volatile MQTTClient_deliveryToken deliveredtoken;

    static void delivered(void *context, MQTTClient_deliveryToken dt, void *ud);
    static int msgarrvd(void *context, char *topicName, int topicLen, MQTTClient_message *message, void *ud);
    static void connlost(void *context, char *cause, void *ud);

    std::map<std::string, topic_t> sud_topic;

public:
    int init(std::string _address, std::string _clientId);
    int uninit();
    int connect(std::string _username, std::string _password);
    int reconnection();
    int publish(std::string _topicName, int _qos, std::string _message);
    int subscribe(std::string _topicName, int _qos, TopicMessageCallback _callback);
    bool topic_match(std::string acl, std::string sub, bool expected = true);
};

#endif
