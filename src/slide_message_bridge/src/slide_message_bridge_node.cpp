#include "slide_message_bridge_node.hpp"

namespace slide_message_bridge
{
    CSlideMessageBridgeNode::CSlideMessageBridgeNode(NodeOptions _node_options)
        : node_options(_node_options)
    {
        mqtt_init(node_options.matt_address, node_options.matt_clientId,
                  node_options.matt_username, node_options.matt_password);

        LaunchPublishers();
        LaunchSubscribers();
        LaunchServices();
        LaunchClinets();
        LaunchActions();
    }

    CSlideMessageBridgeNode::~CSlideMessageBridgeNode()
    {
        control_mode_publisher = nh.advertise<fixed_msg::control_mode>(node_options.control_mode_pub, 1);
    }

    void CSlideMessageBridgeNode::LaunchPublishers()
    {
    }

    void CSlideMessageBridgeNode::LaunchSubscribers()
    {
        task_status_subscriber = nh.subscribe(node_options.task_status_sub, 1, &CSlideMessageBridgeNode::task_status_Callback, this);
        ir1_colorize_info_subscriber = nh.subscribe(node_options.ir1_colorize_info_sub, 1, &CSlideMessageBridgeNode::ir1_colorize_info_Callback, this);
        ir2_colorize_info_subscriber = nh.subscribe(node_options.ir2_colorize_info_sub, 1, &CSlideMessageBridgeNode::ir2_colorize_info_Callback, this);
    }

    void CSlideMessageBridgeNode::LaunchServices()
    {
        /* code */
    }

    void CSlideMessageBridgeNode::LaunchClinets()
    {
        task_data_client = nh.serviceClient<fixed_msg::task>(node_options.task_data_client);
        task_control_client = nh.serviceClient<fixed_msg::task_control>(node_options.task_control_client);
    }

    void CSlideMessageBridgeNode::LaunchActions()
    {
        /* code */
    }

    void CSlideMessageBridgeNode::LaunchSynchronizers()
    {
        /* code */
    }

    void CSlideMessageBridgeNode::publisher_control_mode(int device_id, int mode)
    {
        fixed_msg::control_mode msg;
        msg.device_id = device_id;
        msg.mode = mode;
        control_mode_publisher.publish(msg);
    }

    void CSlideMessageBridgeNode::task_status_Callback(const fixed_msg::task_status::ConstPtr &_msg)
    {
        Json::Value data;
        data["robotId"] = _msg->device_id;
        data["taskHistoryId"] = _msg->task_id;
        data["taskId"] = _msg->task_id;
        data["taskStatus"] = _msg->task_status;
        // data["message"] = _msg->data;
        std::string data_str = data.toStyledString();
        TaskStatusUp_mqttPublish(data_str);
    }

    void CSlideMessageBridgeNode::ir1_colorize_info_Callback(const streaming_service_msgs::ColorizeInfo::ConstPtr &_msg)
    {
        Json::Value data;
        data["camera_id"] = _msg->camera_id;
        data["rtsp_url"] = _msg->rtsp_url;
        data["min_celcius"] = _msg->min_celcius;
        data["max_celcius"] = _msg->max_celcius;
        data["type"] = _msg->type;
        std::string data_str = data.toStyledString();
        ColorizeInfoUp_mqttPublish(data_str);
    }

    void CSlideMessageBridgeNode::ir2_colorize_info_Callback(const streaming_service_msgs::ColorizeInfo::ConstPtr &_msg)
    {
        Json::Value data;
        data["camera_id"] = _msg->camera_id;
        data["rtsp_url"] = _msg->rtsp_url;
        data["min_celcius"] = _msg->min_celcius;
        data["max_celcius"] = _msg->max_celcius;
        data["type"] = _msg->type;
        std::string data_str = data.toStyledString();
        ColorizeInfoUp_mqttPublish(data_str);
    }

    bool CSlideMessageBridgeNode::call_task_data_service(int device_id, std::string data)
    {
        bool bRet = false;
        fixed_msg::task msg;
        msg.request.device_id = device_id;
        msg.request.plan = data;
        if (task_data_client.call(msg))
        {
            ROS_INFO_STREAM("call_task_data_service response:\n"
                            << msg.response);
            if (msg.response.status)
            {
                bRet = true;
            }
        }
        else
        {
            ROS_ERROR("call_task_data_service failed");
        }

        return bRet;
    }

    bool CSlideMessageBridgeNode::call_task_control_service(int device_id, int task_history_id, int flag)
    {
        bool bRet = false;
        fixed_msg::task_control msg;
        msg.request.device_id = device_id;
        msg.request.task_id = task_history_id;
        msg.request.flag = flag;
        if (task_control_client.call(msg))
        {
            ROS_INFO_STREAM("call_task_control_service response:\n"
                            << msg.response);
            if (msg.response.success)
            {
                bRet = true;
            }
        }
        else
        {
            ROS_ERROR("call_task_control_service failed");
        }

        return bRet;
    }

    // mqtt
    int CSlideMessageBridgeNode::mqtt_init(std::string _address, std::string _clientId,
                                           std::string _username, std::string _password)
    {
        // mqtt
        mqtt_base.init(_address, _clientId);
        mqtt_base.connect(_username, _password);

        MqttPublishers();
        MqttSubscribers();

        return 0;
    }

    void CSlideMessageBridgeNode::MqttPublishers()
    {
    }

    void CSlideMessageBridgeNode::MqttSubscribers()
    {
        mqtt_base.subscribe(node_options.mqtt_task_data, 0,
                            std::bind(&CSlideMessageBridgeNode::TaskDataDown_mqttCallback, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
        mqtt_base.subscribe(node_options.mqtt_task_command, 0,
                            std::bind(&CSlideMessageBridgeNode::TaskCommandDown_mqttCallback, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
        mqtt_base.subscribe(node_options.mqtt_control_mode, 0,
                            std::bind(&CSlideMessageBridgeNode::ControlModeDown_mqttCallback, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    }

    int CSlideMessageBridgeNode::Message_mqttPublish(std::string topic, std::string data)
    {
        Json::Reader reader;
        Json::Value data_json;
        if (!reader.parse(data, data_json))
        {
            std::cout << "json parse error" << std::endl;
            return -1;
        }
        time_t stamp = GetTimeStamp();
        std::string strStamp = std::to_string(stamp);
        std::string strLogId = strStamp;
        char md_c[32 + 1];
        if (md5_encoded(strStamp.c_str(), md_c) == 0)
        {
            strLogId = md_c;
        }
        Json::Value mqtt_json;
        mqtt_json["logId"] = strLogId;
        mqtt_json["gatewayId"] = node_options.device_id;
        mqtt_json["timestamp"] = (Json::Int64)stamp;
        mqtt_json["type"] = 0;
        mqtt_json["version"] = MQTT_BASE_VERSION;
        mqtt_json["data"] = data_json;
        std::string mqtt_str = mqtt_json.toStyledString();

        mqtt_base.publish(topic, 0, mqtt_str);

        return 0;
    }

    void CSlideMessageBridgeNode::TaskDataDown_mqttCallback(const std::string &_topicName, const std::string &_message, const int &_length)
    {
        // printf("TaskDataDown_mqttCallback - %s", _message.c_str());

        Json::Reader reader;
        Json::Value root;
        if (!reader.parse(_message, root))
        {
            std::cout << "json parse error" << std::endl;
            return;
        }
        else
        {
            std::string device_id = root["gatewayId"].asString();
            Json::Value data = root["data"];
            if (!data.isNull())
            {
                Json::Value taskData = data["taskData"];
                if (!taskData.isNull())
                {
                    std::string task_str = taskData.toStyledString();
                    call_task_data_service(atoi(device_id.c_str()), task_str);
                }
            }
        }
    }

    void CSlideMessageBridgeNode::ControlModeDown_mqttCallback(const std::string &_topicName, const std::string &_message, const int &_length)
    {
        // printf("ControlModeDown_mqttCallback - %s", _message.c_str());

        Json::Reader reader;
        Json::Value root;
        if (!reader.parse(_message, root))
        {
            std::cout << "json parse error" << std::endl;
            return;
        }
        else
        {
            std::string device_id = root["gatewayId"].asString();
            Json::Value data = root["data"];
            if (!data.isNull())
            {
                int robotId = data["robot_id"].asInt();
                int mode = data["mode"].asInt();
                publisher_control_mode(atoi(device_id.c_str()), mode);
            }
        }
    }

    void CSlideMessageBridgeNode::TaskCommandDown_mqttCallback(const std::string &_topicName, const std::string &_message, const int &_length)
    {
        // printf("TaskCommandDown_mqttCallback - %s", _message.c_str());

        Json::Reader reader;
        Json::Value root;
        if (!reader.parse(_message, root))
        {
            std::cout << "json parse error" << std::endl;
            return;
        }
        else
        {
            std::string device_id = root["gatewayId"].asString();
            Json::Value data = root["data"];
            if (!data.isNull())
            {
                int robotId = data["robotId"].asInt();
                int taskHistoryId = data["taskHistoryId"].asInt();
                int flag = data["flag"].asInt();
                call_task_control_service(robotId, taskHistoryId, flag);
            }
        }
    }

    int CSlideMessageBridgeNode::TaskStatusUp_mqttPublish(std::string data)
    {
        // std::string topic_name = "/ydcloud/gw/" + node_options.device_id + "/iot/task/status/up";
        return Message_mqttPublish(node_options.mqtt_task_status, data);
    }

    int CSlideMessageBridgeNode::ColorizeInfoUp_mqttPublish(std::string data)
    {
        // std::string topic_name = "/ydcloud/gw/" + node_options.device_id + "/iot/task/status/up";
        return Message_mqttPublish(node_options.mqtt_colorize_info, data);
    }

}
