#include "localize_node.h"
#include <limits>
#include <iostream>
#include <fstream>
#include <thread>

#include <boost/filesystem.hpp>
#include <chrono>
#include <Eigen/Core>
#include "eigen3/Eigen/Dense"
#include <opencv2/core.hpp>
#include "opencv2/core/eigen.hpp"


#include "ros/package.h"

#include "gflags/gflags.h"
#include "glog/logging.h"
#include "absl/strings/str_split.h"


#include "localize_service/node_constants.h"


using namespace std;
namespace fs = boost::filesystem;

typedef Eigen::Matrix<double, 3, 3> Mat33;
typedef Eigen::Matrix<double, 3, 1> Vec3;

namespace localize_service {

    namespace {
        constexpr int kInfiniteSubscriberQueueSize = 0;
// Subscribes to the 'topic' for 'trajectory_id' using the 'node_handle' and
// calls 'handler' on the 'node' to handle messages. Returns the subscriber.
        template <typename MessageType>
        ::ros::Subscriber SubscribeWithHandler(
                void (LocalizeNode::*handler)(const std::string&,
                                              const typename MessageType::ConstPtr&),
                const std::string& topic,
                ::ros::NodeHandle* const node_handle, LocalizeNode* const node) {
            return node_handle->subscribe<MessageType>(
                    topic, kInfiniteSubscriberQueueSize,
                    boost::function<void(const typename MessageType::ConstPtr&)>(
                    [node, handler,
                            topic](const typename MessageType::ConstPtr& msg) {
                        (node->*handler)(topic, msg);
                    }));
        }

// Subscribes to the 'topic' for 'trajectory_id' using the 'node_handle' and
// calls 'handler' on the 'node' to handle messages. Returns the subscriber.
        void Read_main_CalibrationFile(std::string _choose_file,
                                 cv::Mat &color_RT, float& O_h,float& O_v)
        {
            cv::FileStorage fs(_choose_file, cv::FileStorage::READ);
            if (!fs.isOpened())
            {
                std::cout << "Cannot open file calibration file" << _choose_file << std::endl;
            }
            else
            {
                fs["CameraExtrinsicMat"] >> color_RT;
                fs["O_h"] >> O_h;
                fs["O_v"] >> O_v;
            }
        }
        void Read_ir_main_CalibrationFile(std::string _choose_file,
                                 cv::Mat &out_RT, cv::Mat &out_intrinsic, cv::Mat &out_dist_coeff, cv::Size &imagesize)
        {
            //std::cout << "_choose_file: " << _choose_file << std::endl;
            cv::FileStorage fs(_choose_file, cv::FileStorage::READ);
            if (!fs.isOpened())
            {
                std::cout << "Cannot open file calibration file" << _choose_file << std::endl;
            }
            else
            {
                fs["CameraMat"] >> out_intrinsic;
                fs["DistCoeff"] >> out_dist_coeff;
                fs["CameraExtrinsicMat"] >> out_RT;
                fs["ImageSize"] >> imagesize;
            }
        }
    }

    LocalizeNode::LocalizeNode() :
            location_state_(LocalizeState::IDLE),
            calib_file_updated_(false)
    {
        
        ::ros::NodeHandle private_nh("~");

        private_nh.param<std::string>("localize_service/map_frame", map_frame_, "map");
        private_nh.param<std::string>("localize_service/ir1_frame", ir1_frame_, "ircam_1_pose");
        private_nh.param<std::string>("localize_service/ir2_frame", ir2_frame_, "ircam_2_pose");

        std::string rail_pose_topic;
        private_nh.param<std::string>("rail_control/rail_pose_topic", rail_pose_topic, "/slide/device/position");

        ros::param::get("~main_calib_filepath", main_calib_filepath_);
        ros::param::get("~ir1_calib_filepath", ir1_calib_filepath_);
        ros::param::get("~ir2_calib_filepath", ir2_calib_filepath_);

        service_servers_.push_back(node_handle_.advertiseService(
                kUpdateCalibParamsName, &LocalizeNode::HandleUpdateCalibParamsService, this));

        service_servers_.push_back(node_handle_.advertiseService(
                kTransformMapToRailServiceName, &LocalizeNode::HandleTransformCoordinate, this));


        Read_main_CalibrationFile(main_calib_filepath_, color_map_exRT_init, init_O_h,init_O_v);
        Read_ir_main_CalibrationFile(ir1_calib_filepath_,ir1_color_exRT,ir1projection_matrix, ir1distortion_matrix,ir1imagesize);
        Read_ir_main_CalibrationFile(ir2_calib_filepath_,ir2_color_exRT,ir2projection_matrix, ir2distortion_matrix,ir2imagesize);
        InitRotateMatrix();
        rail_pose_subscriber_ = SubscribeWithHandler<geometry_msgs::PoseStamped>(
                &LocalizeNode::HandleRailPose, rail_pose_topic, &node_handle_,
                this); }

    LocalizeNode::~LocalizeNode() { }
    void LocalizeNode::InitRotateMatrix()
    {
        LOG(INFO) << "InitRotateMatrix . " ;

        if(calib_file_updated_) {
            Read_main_CalibrationFile(main_calib_filepath_, color_map_exRT_init, init_O_h,init_O_v);
            Read_ir_main_CalibrationFile(ir1_calib_filepath_,ir1_color_exRT,ir1projection_matrix, ir1distortion_matrix,ir1imagesize);
            Read_ir_main_CalibrationFile(ir2_calib_filepath_,ir2_color_exRT,ir2projection_matrix, ir2distortion_matrix,ir2imagesize);
            calib_file_updated_ = false;
        }
        cv::Mat color_map_exRT0_inv = cv::Mat::zeros(4, 4, CV_64F);
        cv::Mat ir_color_exRT_inv = cv::Mat::zeros(4, 4, CV_64F);
        cv::Mat adjustmatrix = cv::Mat::zeros(4, 4, CV_64F);
        Eigen::Matrix4d move_matrix;
        Eigen::Matrix4d trans;
        Eigen::Affine3d temp;
        cv::Mat color_map_exRT0 = color_map_exRT_init.clone();
        cv::Mat ir_color_exRT = ir1_color_exRT.clone();
        cv::cv2eigen(color_map_exRT0, trans);
        temp.matrix() = trans;
        trans = temp.inverse().matrix();
        cv::eigen2cv(trans, color_map_exRT0_inv);

        cv::cv2eigen(ir_color_exRT, trans);
        temp.matrix() = trans;
        trans = temp.inverse().matrix();
        cv::eigen2cv(trans, ir_color_exRT_inv);
        move_matrix<< 1., 0., 0., init_O_h,
                0., 1., 0., init_O_v,
                0., 0., 1., 0.,
                0., 0., 0., 1.;
        cv::eigen2cv(move_matrix, adjustmatrix);
        Eigen::Isometry3d RT1_trans = Eigen::Isometry3d::Identity();
        Eigen::Isometry3d RT2_trans = Eigen::Isometry3d::Identity();
        Eigen::Isometry3d RT3_trans = Eigen::Isometry3d::Identity();

        cv::cv2eigen(color_map_exRT0_inv, RT1_trans.matrix());
        cv::cv2eigen(adjustmatrix, RT2_trans.matrix());
        cv::cv2eigen(ir_color_exRT_inv, RT3_trans.matrix());

        Eigen::Isometry3d delta = RT3_trans *RT2_trans * RT1_trans;
        cv::eigen2cv(delta.matrix(), ir1_map_RT);
        cv::cv2eigen(ir1_map_RT, trans);
        temp.matrix() = trans;
        trans = temp.inverse().matrix();
        cv::eigen2cv(trans, ir1_map_RT);
    }

    void LocalizeNode::HandleRailPose(const std::string& topic, const geometry_msgs::PoseStamped::ConstPtr &rail_pose)
    {
        if(calib_file_updated_) {
            Read_main_CalibrationFile(main_calib_filepath_, color_map_exRT_init, init_O_h,init_O_v);
            Read_ir_main_CalibrationFile(ir1_calib_filepath_,ir1_color_exRT,ir1projection_matrix, ir1distortion_matrix,ir1imagesize);
            Read_ir_main_CalibrationFile(ir2_calib_filepath_,ir2_color_exRT,ir2projection_matrix, ir2distortion_matrix,ir2imagesize);
            calib_file_updated_ = false;
        }

        auto &msg = rail_pose;
        cv::Mat color_map_exRT_init_inv = cv::Mat::zeros(4, 4, CV_64F);
        cv::Mat ir1_color_exRT_inv = cv::Mat::zeros(4, 4, CV_64F);
        cv::Mat ir2_color_exRT_inv = cv::Mat::zeros(4, 4, CV_64F);
        cv::Mat adjustmatrix = cv::Mat::zeros(4, 4, CV_64F);
        cv::Mat map_ir1_RT=cv::Mat::zeros(4, 4, CV_64F);
        cv::Mat map_ir2_RT=cv::Mat::zeros(4, 4, CV_64F);
        Eigen::Matrix4d move_matrix;
        Eigen::Matrix4d trans;
        Eigen::Affine3d temp;
        cv::Mat color_map_exRT0 = color_map_exRT_init.clone();
        cv::Mat ir1_color_exRT0 = ir1_color_exRT.clone();
        cv::Mat ir2_color_exRT0 = ir2_color_exRT.clone();
        cv::cv2eigen(color_map_exRT0, trans);
        temp.matrix() = trans;
        trans = temp.inverse().matrix();
        cv::eigen2cv(trans, color_map_exRT_init_inv);//初始位置，地图到可见光的外参

        cv::cv2eigen(ir1_color_exRT, trans);
        temp.matrix() = trans;
        trans = temp.inverse().matrix();
        cv::eigen2cv(trans, ir1_color_exRT_inv);//可见光到红外1的外参

        cv::cv2eigen(ir2_color_exRT, trans);
        temp.matrix() = trans;
        trans = temp.inverse().matrix();
        cv::eigen2cv(trans, ir2_color_exRT_inv);//可见光到红外2的外参

        float H_move=init_O_h-msg->pose.position.x;
        float V_move=init_O_v-msg->pose.position.y;
        move_matrix<< 1., 0., 0., H_move,
                0., 1., 0., V_move,
                0., 0., 1., 0.,
                0., 0., 0., 1.;

        cv::eigen2cv(move_matrix, adjustmatrix);
        Eigen::Isometry3d RT1_trans = Eigen::Isometry3d::Identity();
        Eigen::Isometry3d RT2_trans = Eigen::Isometry3d::Identity();
        Eigen::Isometry3d RT3_trans = Eigen::Isometry3d::Identity();
        Eigen::Isometry3d RT4_trans = Eigen::Isometry3d::Identity();

        cv::cv2eigen(color_map_exRT_init_inv, RT1_trans.matrix());
        cv::cv2eigen(adjustmatrix, RT2_trans.matrix());
        cv::cv2eigen(ir1_color_exRT_inv, RT3_trans.matrix());
        cv::cv2eigen(ir2_color_exRT_inv, RT4_trans.matrix());

        Eigen::Isometry3d delta1 = RT3_trans *RT2_trans * RT1_trans;
        Eigen::Isometry3d delta2 = RT4_trans *RT2_trans * RT1_trans;

        cv::eigen2cv(delta1.matrix(), map_ir1_RT);
        cv::cv2eigen(map_ir1_RT, trans);
        temp.matrix() = trans;
        trans = temp.inverse().matrix();
        cv::eigen2cv(trans, ir1_map_RT);
        std::cout<<"ir1->map exRT:"<<std::endl
                 <<ir1_map_RT<<std::endl;
        Eigen::Matrix3d rotation_matrix;
        cv::Mat handin_R = ir1_map_RT(cv::Rect(0, 0, 3, 3));
        cv::cv2eigen(handin_R, rotation_matrix);
        Eigen::Quaterniond eigen_tf(rotation_matrix);
        tf::Quaternion quad(eigen_tf.x(), eigen_tf.y(), eigen_tf.z(), eigen_tf.w());
        tf::Transform transform;
        transform.setOrigin(tf::Vector3(ir1_map_RT.at<double>(0, 3), ir1_map_RT.at<double>(1, 3), ir1_map_RT.at<double>(2, 3)));
        transform.setRotation(quad);
        broadcaster_ir1.sendTransform(tf::StampedTransform(transform, msg->header.stamp, map_frame_, ir1_frame_));

        cv::eigen2cv(delta2.matrix(), map_ir2_RT);
        cv::cv2eigen(map_ir2_RT, trans);
        temp.matrix() = trans;
        trans = temp.inverse().matrix();
        cv::eigen2cv(trans, ir2_map_RT);
        std::cout<<"ir2->map exRT:"<<std::endl
                 <<ir2_map_RT<<std::endl;
        handin_R = ir2_map_RT(cv::Rect(0, 0, 3, 3));
        cv::cv2eigen(handin_R, rotation_matrix);
        eigen_tf = Eigen::Quaterniond(rotation_matrix);
        quad = tf::Quaternion(eigen_tf.x(), eigen_tf.y(), eigen_tf.z(), eigen_tf.w());
        transform.setOrigin(tf::Vector3(ir2_map_RT.at<double>(0, 3), ir2_map_RT.at<double>(1, 3), ir2_map_RT.at<double>(2, 3)));
        transform.setRotation(quad);
        broadcaster_ir2.sendTransform(tf::StampedTransform(transform, msg->header.stamp, map_frame_, ir2_frame_));
    }

    bool LocalizeNode::HandleUpdateCalibParamsService(
            localize_service_msgs::UpdateCalibParams::Request& request,
            localize_service_msgs::UpdateCalibParams::Response& response) {
        LOG(INFO) << "Enter HandleUpdateCalibParamsService . " ;

        ofstream calib_file(main_calib_filepath_);
        calib_file << request.calib_params[0];

        calib_file_updated_ = true;

        response.status.code = LocationResult::SUCCESS;
        LOG(INFO) << "Exit HandleUpdateCalibParamsService . " ;
        return true;
    }


    bool LocalizeNode::HandleTransformCoordinate(
            localize_service_msgs::TransformMapToRail::Request& request,
            localize_service_msgs::TransformMapToRail::Response& response) {
        LOG(INFO) << "Enter HandleTransformCoordinate . " ;
        Eigen::Matrix4d transform;
        Eigen::Affine3d temp;
        cv::Mat current_ir_map_exRT =ir1_map_RT.clone();
        cv::Mat current_ir_map_exRT_inv = cv::Mat::zeros(4, 4, CV_64F);
        cv::Mat move_matrix= cv::Mat::zeros(4, 4, CV_64F);

        current_ir_map_exRT.at<double>(0, 3)=request.map_point.x;
        current_ir_map_exRT.at<double>(1, 3)=request.map_point.y;
        current_ir_map_exRT.at<double>(2, 3)=request.map_point.z;
        cv::cv2eigen(current_ir_map_exRT, transform);
        temp.matrix() = transform;
        transform = temp.inverse().matrix();
        cv::eigen2cv(transform, current_ir_map_exRT_inv);

        Eigen::Isometry3d RT1_trans = Eigen::Isometry3d::Identity();
        Eigen::Isometry3d RT2_trans = Eigen::Isometry3d::Identity();
        Eigen::Isometry3d RT3_trans = Eigen::Isometry3d::Identity();

        cv::cv2eigen(current_ir_map_exRT_inv, RT1_trans.matrix());
        cv::cv2eigen(ir1_color_exRT, RT2_trans.matrix());
        cv::cv2eigen(color_map_exRT_init, RT3_trans.matrix());

        Eigen::Isometry3d delta = RT2_trans * RT1_trans * RT3_trans;
        cv::eigen2cv(delta.matrix(), move_matrix);

        cv::cv2eigen(move_matrix, transform);
        temp.matrix() = transform;
        transform = temp.inverse().matrix();
        cv::eigen2cv(transform, move_matrix);

        cv::Mat handin_T = move_matrix(cv::Rect(3, 0, 1, 3));
        std::cout<<"out motor move: "<<std::endl<<handin_T.at<double>(0, 0)+init_O_h
                 <<std::endl<<handin_T.at<double>(1, 0)+init_O_v<<std::endl;
        response.rail_point.x=handin_T.at<double>(0, 0)+init_O_h;
        response.rail_point.y=handin_T.at<double>(1, 0)+init_O_v;
        response.rail_point.z=0.0f;

        response.status.code = LocationResult::SUCCESS;
        LOG(INFO) << "Exit HandleTransformCoordinate . " ;
        return true;
    }

}



