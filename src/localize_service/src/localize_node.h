#pragma once

#include <memory>
#include <vector>
#include <thread>

#include <opencv2/core.hpp>

#include "ros/ros.h"
#include <tf/transform_broadcaster.h>
#include <tf/transform_datatypes.h>

#include "localize_service_msgs/UpdateCalibParams.h"
#include "localize_service_msgs/TransformMapToRail.h"


namespace  localize_service{
class LocalizeNode {
public:
  enum LocationResult {
      SUCCESS = 0,
      IO_ERROR = -3,
      BUSY = -4,
      ERROR = -5

  };
  LocalizeNode();
  ~LocalizeNode();

  LocalizeNode(const LocalizeNode&) = delete;
  LocalizeNode& operator=(const LocalizeNode&) = delete;
private:
  enum LocalizeState {
      IDLE = 0,
      LOCALIZING = 1,
  };
  void InitRotateMatrix();
  void HandleRailPose(const std::string& topic, const geometry_msgs::PoseStamped::ConstPtr &rail_pose);
  bool HandleUpdateCalibParamsService(
          localize_service_msgs::UpdateCalibParams::Request& request,
          localize_service_msgs::UpdateCalibParams::Response& response) ;

  bool HandleTransformCoordinate(
      localize_service_msgs::TransformMapToRail::Request& request,
      localize_service_msgs::TransformMapToRail::Response& response);

  ::ros::NodeHandle node_handle_;
  std::vector<::ros::ServiceServer> service_servers_;

  ::ros::Subscriber  rail_pose_subscriber_;

  std::string main_calib_filepath_,ir1_calib_filepath_,ir2_calib_filepath_;

  std::string map_frame_;
  std::string ir1_frame_,ir2_frame_;

  int location_state_ = LocalizeState::IDLE;
  bool stop_flag_ = false;
  tf::TransformBroadcaster broadcaster_ir1;
  tf::TransformBroadcaster broadcaster_ir2;

  cv::Mat color_map_exRT_init, ir1_color_exRT,ir1projection_matrix, ir1distortion_matrix;
  cv::Mat ir2_color_exRT,ir2projection_matrix, ir2distortion_matrix;
  cv::Size ir1imagesize,ir2imagesize;
  float init_O_h,init_O_v,in_H_move,in_V_move;
  //cv::Mat final_RT=cv::Mat::zeros(4, 4, CV_64F);
  cv::Mat ir1_map_RT=cv::Mat::zeros(4, 4, CV_64F);
  cv::Mat ir2_map_RT=cv::Mat::zeros(4, 4, CV_64F);
  bool calib_file_updated_;
};
  
}