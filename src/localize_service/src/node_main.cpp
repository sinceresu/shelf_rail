#include <algorithm>
#include <fstream>
#include <iostream>

#include "gflags/gflags.h"
#include "glog/logging.h"

#include "localize_node.h"

using namespace std;

namespace localize_service {
void Run(int argc, char** argv) {
  LOG(INFO) << "start localize service.";

  LocalizeNode  location_node;

  ::ros::spin();

  LOG(INFO) << "finished localize service.";

}

}


int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);
  FLAGS_logtostderr = true;

  ::ros::init(argc, argv, "localize_service_node");
  
  localize_service::Run(argc, argv);

}


