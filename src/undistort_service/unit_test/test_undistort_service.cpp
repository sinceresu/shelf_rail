
#include <stdio.h>
#include <thread> 

#include <opencv2/opencv.hpp>

#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/Image.h>

#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

#include "tf2_eigen/tf2_eigen.h"
#include <tf_conversions/tf_eigen.h>
#include "tf2_ros/transform_broadcaster.h"
#include "tf2_geometry_msgs/tf2_geometry_msgs.h"

#include "gflags/gflags.h"
#include "glog/logging.h"
DEFINE_string(topics, "",

              "First directory in which configuration files are searched, "
              "second is always the Cartographer installation to allow "
              "including files from there.");



#include <memory>
using namespace std;
namespace undistort_service{
namespace {

void Undistort(int argc, char** argv) {
    ::ros::NodeHandle node_handle_;

  Eigen::Affine3d camera_to_lidar = Eigen::Affine3d::Identity();
  geometry_msgs::TransformStamped transform_msg = tf2::eigenToTransform(camera_to_lidar);
  transform_msg.header.stamp = ros::Time::now();
  transform_msg.header.frame_id = "map";
  transform_msg.child_frame_id = "lidar_pose";
  tf2_ros::TransformBroadcaster broadcaster_;
  broadcaster_.sendTransform(transform_msg);

  std::this_thread::sleep_for(std::chrono::milliseconds(50));

	cv::Mat raw_image(384, 288, CV_32F, 10 );
  ::std_msgs::Header header;
  header.stamp = ros::Time::now();
  header.frame_id = "ir_raw";
  cv_bridge::CvImage heat_cvimg(header,
                                sensor_msgs::image_encodings::TYPE_32FC1, raw_image);
  ros::Publisher temperature_buffer_pub_  = node_handle_.advertise<sensor_msgs::Image>("/fixed/infrared/raw", 1);

  temperature_buffer_pub_.publish(heat_cvimg.toImageMsg());

  std::this_thread::sleep_for(std::chrono::milliseconds(50));
  transform_msg.header.stamp = ros::Time::now();
  broadcaster_.sendTransform(transform_msg);


  
  ::ros::spin();


}

}
}


int main(int argc, char** argv) {
  tf2::Quaternion a_quaternion(0.710197214042,-0.703892254089,0.00762120898607,0.00987567585574);
  double a_roll, a_pitch, a_yaw;
  tf2::Matrix3x3(a_quaternion).getRPY(a_roll, a_pitch, a_yaw); //进行转换

  std::cout << Eigen::Quaterniond(0.710225976449,-0.0075323985233,0.00994350991271,-0.703863233846).toRotationMatrix() << std::endl;

  tf2::Quaternion b_quaternion(-0.00476519525605,-0.00441343267176,0.705433517003,0.708746335169);
  double b_roll, b_pitch, b_yaw;
  tf2::Matrix3x3(b_quaternion).getRPY(b_roll, b_pitch, b_yaw); //进行转换

  std::cout << Eigen::Quaterniond(-0.00443573685532,0.708748475742,-0.705431366377,-0.00474443925961).toRotationMatrix() << std::endl;

  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);
  FLAGS_logtostderr = true;
  LOG(INFO) << "start test_undistort_service.";

  ros::init(argc, argv, "test_undistort_service");
  ros::start();
  undistort_service::Undistort(argc, argv);

  getchar();
 
}

