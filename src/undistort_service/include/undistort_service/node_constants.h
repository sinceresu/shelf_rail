/*
 * Copyright 2016 Sujin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef UNDISTORT_SERVICE_NODE_CONSTANTS_H
#define UNDISTORT_SERVICE_NODE_CONSTANTS_H

#include <string>
#include <vector>
namespace undistort_service {

// Default topic names; expected to be remapped as needed.
constexpr char kSetMapFileServiceName[] = "undistort_service/set_map";
constexpr char kUpdateCalibParamsServiceName[] = "undistort_service/update_calib_params";
constexpr char kStartUndistortServiceName[] = "undistort_service/start_undistort";
constexpr char kStopUndistortServiceName[] = "undistort_service/stop_undistort";


}  // namespace cartographer_ros

#endif  // COLORING_SERVICE_NODE_CONSTANTS_H
