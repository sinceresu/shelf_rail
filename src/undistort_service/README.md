<!--
 * @Descripttion: 
 * @version: 
 * @Author: li
 * @Date: 2021-05-12 16:14:16
 * @LastEditors: li
 * @LastEditTime: 2021-05-12 18:18:01
-->
# undistort_service
接收红外驱动发出的温度图像以及定位服务提供的红外相机位姿，对两者进行时间同步，打包发送出位姿对齐的温度图像。 

##  订阅话题
###  红外温度图片
Topic: /fixed/infrared/raw 

Type: sensor_msgs::Image


##  发布话题
###  位姿对齐温度图片
Topic: /infrared/undistorted 

Type: undistort_service_msgs::PosedImage

undistort_service_msgs::PosedImage定义：
```json
sensor_msgs/Image image   #温度图片 
geometry_msgs/Pose pose   # 当前位姿 
sensor_msgs/CameraInfo camera_info  #相机参数
```

##  服务
###  设置相机参数
Service Name: undistort_service/update_calib_params  
Type: undistort_service_msgs::update_calib_params  

undistort_service_msgs::update_calib_params定义：
```json
string[] calib_params   #相机标定参数
---
undistort_service_msgs/StatusResponse status
```
###  开始对齐任务
Service Name: undistort_service/start_undistort 

Type: undistort_service_msgs::start_undistort

```json
---
undistort_service_msgs/StatusResponse status
```
###  停止对齐任务
Service Name: undistort_service/stop_undistort  

Type: undistort_service_msgs::stop_undistort 

undistort_service_msgs::stop_undistort定义：
```json
---
undistort_service_msgs/StatusResponse status
```
##  启动服务
###  启动着色对齐服务

```shell
#driver
roslaunch undistort_service undistort_service.launch
```
