# shelf_rail

中交信红外监控项目。

## 安装驱动


## 安装编译环境
```sh
sudo apt-get install -y \
    libeigen3-dev \
    libgflags-dev \
    libgoogle-glog-dev

```
进行编译
进行shelf_rail目录，执行：
```sh
catkin_make install

```


在目标机执行
将“install”目录拷贝到目标机对应的workspace目录中，进入该workspace目录后执行

```sh
source install/setup.bash
roslaunch ./start_all.launch

```


